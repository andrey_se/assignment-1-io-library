section .data
    space   db 0x20, 0x0A, 0x09  ; Пробельные символы
    %define SYMB_NEW_LINE 0xA
    
    %define SYS_READ 0
    %define SYS_WRITE 1
    %define SYS_EXIT 60

    %define STDIN 0
    %define STDOUT 1

    %define LEN_CHAR 1


section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .length_loop:
        inc rax
        cmp byte[rdi+rax-1], 0
        jne .length_loop
    dec rax
    ret
    

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi

    mov rdx, rax    ; записали длину строки
    mov rax, SYS_WRITE  ; syscall для вывода в stdout
    mov rdi, 1  ; передаём адрес начала строки в stdout
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, SYS_WRITE  ; syscall для вывода в stdout
    mov rdi, 1  ; передаём адрес начала строки в stdout
    mov rdx, LEN_CHAR  ; длина строки (1 символ)
    mov  rsi, rsp
    syscall
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, SYMB_NEW_LINE


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rdi, rsp
    sub rsp, 24             ; выделяем место на стеке
    mov byte[rdi - 1], 0
    dec rdi
    mov r10, 10
    .print_loop:
        xor rdx, rdx
        div r10
        add rdx, '0'
        dec rdi
        mov [rdi], dl
        test rax, rax
        jne .print_loop
    .end:
        call print_string
        add rsp, 24
        ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jge .pos_sign
    ; Если число отрицательное, выведем минус и инвертируем его
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .pos_sign:
        call print_uint
        ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    mov rax, 1
    .equal_loop:
        mov al, byte [rdi]
        cmp al, [rsi]
        jne .notEqual
        test al, al
        je .equal
        inc rdi
        inc rsi
        jmp .equal_loop
    .equal:
        mov rax, 1
        ret
    .notEqual:
        xor rax, rax
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:    
    push 0
    mov rsi, rsp        ; указатель на буфер для прочитанных данных
    mov rdx, LEN_CHAR          ; количество байт для чтения (в данном случае, один символ)
    mov rax, SYS_READ          ; syscall для чтения
    mov rdi, STDIN          ; файловый дескриптор stdin
    syscall
    
    test rax, rax       ; Проверяем код возврата
    js .read_error      ; Если отрицательный, значит произошла ошибка

    ; успешное чтение, продолжаем выполнение
    pop rax
    ret

    .read_error:
        mov rax, -1          ; устанавливаем значение ошибки
        ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r13
    push r14
    push r15
    push rdi
    mov r13, rdi
    mov r14, rsi
    xor r15, r15
    dec r14  

    .skip_whitespace:
        ; Читаем один символ из stdin
        call read_char
        cmp al, ` `
        je .skip_whitespace
        cmp al, `\t`
        je .skip_whitespace
        cmp al, `\n`
        je .skip_whitespace
        test al, al
        je .word_done

    .read_into_buffer:
        test r14, r14
        jle .buffer_full
        mov [r13], al
        inc r13        
        dec r14          
        inc r15
        call read_char
        cmp al, [space]
        je .word_done
        test rax, rax
        je .word_done
        jmp .read_into_buffer
    .word_done:
        pop rax
        mov [r13], byte 0
        mov rdx, r15
        pop r15
        pop r14
        pop r13
        ret
    .buffer_full:
        pop rax
        xor rax, rax
        pop r15
        pop r14
        pop r13
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx: его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov r8, 10
    xor r9, r9
    xor rax, rax
    mov rcx, -1
    xor rdx, rdx

    .reading_char:
        inc rcx
        mov r9b, [rdi+rcx]
        cmp r9b, 0
        je .end
        cmp r9b, '0'
        jl .end
        cmp r9b, '9'
        jg .end
        sub r9b, '0'
        mul r8
        add rax, r9
        jmp .reading_char
    
    .end:
        mov rdx, rcx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .neg
    jmp parse_uint
    .neg:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    test rdx, rdx
    mov rax, rsi
    je .buffer_full
    mov r8b, [rdi]
    mov [rsi], r8b
    test r8b, r8b
    je .end
    dec rdx
    inc rsi
    inc rdi
    jmp string_copy
    .end:
        sub rsi, rax
        mov rax, rsi
        ret
    .buffer_full:
        xor rax, rax
        ret

